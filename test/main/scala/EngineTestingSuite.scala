package main.scala

import org.scalatest.junit._
import java.util.Queue
import org.eclipse.swt.graphics.Point
import java.util.ArrayList
import org.eclipse.swt.widgets.Display
import java.util.concurrent.ConcurrentLinkedQueue

class EngineSuite extends VSpec {
  
  info("Commencing Engine testing")

  feature("Engine collision detection") {
    scenario("One ball collides with another one") {

      Given("an empty engine without timer")
      val engine = new Engine(0, new ConcurrentLinkedQueue[Point], 10, new ArrayList[Renderable], Display.getDefault);

      When("there are two balls in the middle of the field headed at each other")
      val ball1 : Ball = new Ball(10, 20, new Point(400, 400), Math.toDegrees(Math.atan2(400, 450)));
      val ball2 : Ball = new Ball(10, 20, new Point(400, 450), Math.toDegrees(Math.atan2(400, 400)));
      engine.addPredefinedBall(ball1);
      engine.addPredefinedBall(ball2);

      Then("one of those balls should change it's direction in a few ticks")
      val startDirection = ball1.direction;
      for( i <- 1 to 10) {
        engine.tick
      }
      assert(ball1.direction != startDirection);
    }
  }
  
}