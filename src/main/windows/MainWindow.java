package main.windows;

import java.util.Queue;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Canvas;

public class MainWindow extends Composite {
	
	public Canvas canvas = null;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MainWindow(Composite parent, int style, Queue<Point> clickEvents) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		canvas = new Canvas(this, SWT.NONE);
		canvas.setLayout(new FillLayout(SWT.HORIZONTAL));
		GridData gd_canvas = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_canvas.heightHint = 600;
		gd_canvas.widthHint = 600;
		gd_canvas.verticalAlignment = GridData.FILL;
		gd_canvas.horizontalAlignment = GridData.FILL;
		gd_canvas.grabExcessHorizontalSpace = true;
		gd_canvas.grabExcessVerticalSpace = true;
		canvas.setLayoutData(gd_canvas);
		canvas.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				clickEvents.add(new Point(e.x, e.y));
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
		});
		canvas.setBackground(new Color(this.getDisplay(), new RGB(100,100,100)));
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
