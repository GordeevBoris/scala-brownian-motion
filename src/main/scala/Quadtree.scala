package main.scala

import org.eclipse.swt.graphics.Rectangle
import java.util.ArrayList
import java.util.List;
import scala.collection.mutable._
import scala.collection.JavaConverters._

class Quadtree(val level: Int, val bounds: Rectangle,
               val offsetX: Int, val offsetY: Int) {

  val maxObjects: Int = 3;
  val maxLevels: Int = 5;
  var objects: List[Ball] = new ArrayList[Ball]();
  val nodes: Array[Quadtree] = Array(null, null, null, null);
  val subQuadtree = if (level == maxLevels) null
  else (new Quadtree(level + 1, _: Rectangle, offsetX, offsetY));
  val verticalMidpoint: Double = bounds.x + (bounds.width / 2);
  val horizontalMidpoint: Double = bounds.y + (bounds.height / 2);
  val top = bounds.height;
  val right = bounds.width;
  val bottom = bounds.y;
  val left = bounds.x;

  def clear: Unit = {
    objects.clear();
    nodes.transform { x => if (x != null) { x.clear }; null }
  }

  def split = if (subQuadtree != null) {
    val subWidth = bounds.width / 2;
    val subHeight = bounds.height / 2;
    nodes(0) = subQuadtree(new Rectangle(verticalMidpoint.ceil.toInt, bottom, subWidth, subHeight));
    nodes(1) = subQuadtree(new Rectangle(left, bottom, subWidth, subHeight));
    nodes(2) = subQuadtree(new Rectangle(left, horizontalMidpoint.ceil.toInt, subWidth, subHeight));
    nodes(3) = subQuadtree(new Rectangle(verticalMidpoint.ceil.toInt, horizontalMidpoint.ceil.toInt, subWidth, subHeight));
  }

  def getIndex(figure: Ball): Int = {
    val topQuadrant: Boolean = figure.fitsY(top) && figure.fitsY(horizontalMidpoint);
    val bottomQuadrant: Boolean = figure.fitsY(bottom) && figure.fitsY(horizontalMidpoint);
    val leftQuadrant: Boolean = figure.fitsX(left) && figure.fitsX(verticalMidpoint);
    val rightQuadrant: Boolean = figure.fitsX(right) && figure.fitsX(verticalMidpoint);
    if (leftQuadrant) {
      if (topQuadrant) { 2 }
      else if (bottomQuadrant) { 1 }
      else { -1 }
    } else if (rightQuadrant) {
      if (topQuadrant) { 3 }
      else if (bottomQuadrant) { 0 }
      else { -1 }
    } else { -1 }
  }

  def insert(pRect: Ball): Unit = if (nodes(0) != null) {
    var index = getIndex(pRect);
    if (index != -1) {
      nodes(index).insert(pRect);
    }
  } else {
    objects.add(pRect);
    if (objects.size() > maxObjects && level < maxLevels) {
      if (nodes(0) == null) {
        split;
      }
      objects = objects.asScala.filter { x => if (getIndex(x) != -1) { nodes(getIndex(x)).insert(x); false } else true }.asJava;
    }
  }
  
  def retrieve(returnObjects : List[Ball] , pRect : Ball) : List[Ball] = {
   if (getIndex(pRect) != -1 && nodes(0) != null) {
     nodes(getIndex(pRect)).retrieve(returnObjects, pRect);
   }
   returnObjects.addAll(objects);
   returnObjects;
 }

}