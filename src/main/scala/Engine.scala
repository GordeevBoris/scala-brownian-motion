package main.scala

import java.util.Timer
import java.util.TimerTask
import java.util.List
import java.util.ArrayList
import java.util.Queue
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.graphics.Rectangle
import scala.collection.JavaConverters._
import scala.util.Random
import org.eclipse.swt.widgets.Display

class Engine(val tickFrequencyFPS: Int,
             val clickEvents: Queue[Point],
             val maximumBalls: Int,
             val renderable : List[Renderable],
             private val drawingCanvas : Display) extends Runnable {

  if (tickFrequencyFPS < 0) {
    throw new IllegalArgumentException("Bad frequency.");
  }
  
  private val tickIntervalMS: Int = (1000 / tickFrequencyFPS).ceil.toInt;
  private val frameTimer: Timer = new Timer();
  private val field : Rectangle = new Rectangle(0,0,1000,1000);
  private val space: Quadtree = new Quadtree(0, field, 5, 5);
  private val objects: List[Ball] = new ArrayList[Ball]();
  
  def run = 
    if (tickFrequencyFPS > 0) 
      frameTimer.schedule(new TimerTask() { def run = tick }, 1000, tickIntervalMS)
    else Unit

  def tick = {
    getUserInput;
    space.clear;
    moveAll;
    renderable.synchronized(updateRenderable(System.currentTimeMillis()));
  }

  private def getUserInput = if (space.objects.size() < maximumBalls && clickEvents.peek() != null) addRandomBall(clickEvents.poll());

  def addRandomBall(point: Point) = {
    val rand : Random = new Random();
    val ball = new Ball(rand.nextInt(5) + 1,
        rand.nextInt(10) + 10,
        point, 
        rand.nextDouble() * 360);
    addPredefinedBall(ball);
  }
  
  def addPredefinedBall(ball: Ball) = {
    space.insert(ball);
    objects.add(ball);
  }

  private def moveAll = objects.asScala.transform { x => x.move; x }.transform { x => space.insert(x); x }.foreach { x => collideWithAll(x) }
  private def collideWithAll(x : Ball) = space.retrieve(new ArrayList[Ball](), x).asScala.foreach { y => doCollision(x,y) }
  private def doCollision(x : Ball, y : Ball) = if (y != x && x.collidesWith(y)) x.collide(y)

  private def updateRenderable(time: Long) = renderable.synchronized({
    renderable.clear();
    objects.asScala.foreach { x => renderable.add(x.convert(drawingCanvas, field)) }
  });
  
  def stop = frameTimer.cancel();
  
}