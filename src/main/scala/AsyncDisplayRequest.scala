package main.scala

import org.eclipse.swt.graphics.GC
import org.eclipse.swt.widgets.Canvas
import java.util.List
import scala.collection.JavaConverters._
import org.eclipse.swt.graphics.Rectangle
import org.eclipse.swt.graphics.Color
import org.eclipse.swt.graphics.RGB

class AsyncDisplayRequest(val renderingCanvas: Canvas, 
                          val renderable: List[Renderable],
                          val renderingField: Rectangle) extends Runnable {
  
  val gray : Color = new Color(renderingCanvas.getDisplay(), new RGB(100,100,100));
  
  def run = renderable.synchronized({
    val drawer: GC = new GC(renderingCanvas);
    drawer.setBackground(gray);
    drawer.fillRectangle(renderingCanvas.getBounds);
    renderable.asScala.foreach { x => drawer.drawImage(x.image, Math.floor(x.positionX*renderingField.width).toInt, Math.floor(x.positionY*renderingField.height).toInt) }
    drawer.dispose();
    renderingCanvas.update();
  });
  
}