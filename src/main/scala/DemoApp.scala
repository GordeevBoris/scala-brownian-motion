package main.scala

import java.util.concurrent.ConcurrentLinkedQueue
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.FillLayout
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.Shell
import main.windows.MainWindow
import java.util.ArrayList
import java.util.Collections
import java.util.List

object DemoApp {
  
  val DEFAULT_SHELL_WIDTH = 600;
  val DEFAULT_SHELL_HEIGHT = 600;
  val DEFAULT_BORDER_OFFSET_WIDTH = 20;
  val DEFAULT_BORDER_OFFSET_HEIGHT = 43;
  val DEFAULT_SHELL_STYLE_NO_RESIZE = SWT.SHELL_TRIM & (~SWT.RESIZE);
  
  def main(args: Array[String]): Unit = {
    start;
  }
  
  def start = {
    val clickEvents = new ConcurrentLinkedQueue[Point]();
    val display = new Display();
    val shell = new Shell(display, DEFAULT_SHELL_STYLE_NO_RESIZE);
    shell.setSize(DEFAULT_SHELL_WIDTH + DEFAULT_BORDER_OFFSET_WIDTH
        , DEFAULT_SHELL_HEIGHT + DEFAULT_BORDER_OFFSET_HEIGHT);
    val MainWindow = new MainWindow(shell, SWT.BORDER, clickEvents);
    val grid = new FillLayout();
		shell.setLayout(grid);
    shell.layout();
    shell.open();
    val renderable: List[Renderable] = Collections.synchronizedList(new ArrayList[Renderable]());
    val renderer : Renderer = new Renderer(MainWindow.canvas, 60, MainWindow.canvas.getBounds, renderable);
    renderer.run();
    val engine : Engine = new Engine(10, clickEvents, 10, renderable, display);
    engine.run();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}

/**
 * @author Boris
 */
class DemoApp {

}