package main.scala

import java.util.Timer
import org.eclipse.swt.widgets.Canvas
import java.util.TimerTask
import java.util.List
import java.util.Collections
import java.util.ArrayList
import scala.collection.JavaConverters._
import org.eclipse.swt.graphics.GC
import org.eclipse.swt.graphics.Rectangle

class Renderer(val renderingCanvas: Canvas,
               val renderingFrequencyFPS: Int,
               val renderingField: Rectangle,
               val renderable: List[Renderable]) extends Runnable {

  if (renderingFrequencyFPS <= 0) {
    throw new IllegalArgumentException("Bad frequency.");
  }

  val renderingIntervalMS: Int = (1000 / renderingFrequencyFPS).ceil.toInt;
  val frameTimer: Timer = new Timer();
  val displayer: AsyncDisplayRequest = new AsyncDisplayRequest(renderingCanvas, renderable, renderingField);

  def run = {
    frameTimer.schedule(new TimerTask() { /*def run = renderable.synchronized(render);*/ 
      def run = renderingCanvas.getDisplay.asyncExec(displayer) },
      1000, renderingIntervalMS);
  }

//  def render = {
//    val drawer: GC = new GC(renderingCanvas);
//    renderable.asScala.foreach { x => drawer.drawImage(x.image, Math.floor(x.positionX*renderingField.width).toInt, Math.floor(x.positionY*renderingField.height).toInt) }
//    drawer.dispose();
//    renderingCanvas.redraw();
//  }

}