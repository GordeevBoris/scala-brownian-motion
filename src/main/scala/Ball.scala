package main.scala

import org.eclipse.swt.graphics.Point
import org.eclipse.swt.graphics.Rectangle
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.graphics.ImageData
import org.eclipse.swt.graphics.RGB
import java.io.InputStream
import java.io.File

object Ball {
  val ballImage : ImageData = new ImageData(Thread.currentThread().getContextClassLoader().getResourceAsStream("ball.gif"));
}

class Ball (var speed: Int,
    val radius : Int,
    private val start : Point,
    var direction : Double) {
  
  if (speed <= 0) {
    throw new IllegalArgumentException("Bad speed");
  }
  
  def moveDirection(diff : Double) = if (direction + diff > 360) direction = 360 - (direction + diff) 
                                      else if (direction + diff < 0) direction = 360 - Math.abs(direction + diff)
                                      else direction = direction + diff
                                      
  var x : Double = start.x;
  var y : Double = start.y;
  
  def move = {
    x = x + speed * Math.cos(Math.toRadians(direction));
    y = y + speed * Math.sin(Math.toRadians(direction));
  }
  
  def fitsX(thatX : Double) : Boolean = Math.abs(thatX - x) < radius
  def fitsY(thatY : Double) : Boolean = Math.abs(thatY - y) < radius
  def distanceTo(that : Ball) : Double = Math.sqrt(Math.pow(this.x - that.x, 2) + Math.pow(this.y - that.y, 2))
  def collidesWith(that : Ball) : Boolean = this.distanceTo(that) < (this.radius + that.radius) && this.headsToward(that)
  def collide(that : Ball) = Unit
  def convert(canvasDisplay : Display, spaceSize : Rectangle) : Renderable = new Renderable(canvasDisplay, Ball.ballImage, radius*2, radius*2, x/spaceSize.width, y/spaceSize.height);
  def headsToward(that : Ball) : Boolean = {
    val relativeHeading = Math.toDegrees(Math.atan2(that.y - this.y, that.x - this.x));
    if (relativeHeading + 90 > this.direction && relativeHeading - 90 < this.direction) true else false;
  }
  
}